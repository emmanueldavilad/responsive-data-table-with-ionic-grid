import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  page  = 0;
  resultsCount = 10;
  totalPages = 10;
  data =[];

  bulkEdit= false;
  sortDirection = 0;
  sortKey: any;
  edit = {};
  constructor(private http: HttpClient) {
    this.loadData();
  }

  private  loadData(){  
    this.http.get(`https://randomuser.me/api/?page=${this.page}&results=${this.resultsCount}`).subscribe(res=>{
    console.log('res_', res);
    this.data = res['results'];
    this.sort();

    })
  }

 public sortBy(key){
   this.sortKey =  key;
   this.sortDirection++
   this.sort();

  }

  private sort(){
    if(this.sortDirection == 1){
      this.data = this.data.sort((a,b) =>{
        console.log('a:', a);
        const valA = a[this.sortKey];
        const valB = b [this.sortKey];
        return valA.localeCompare(valB);
      });
    }else if (this.sortDirection == 2){
      this.data = this.data.sort((a,b) =>{
        console.log('b:', b);
        const valA = a[this.sortKey];
        const valB = b [this.sortKey];
        return valB.localeCompare(valA);
      });
    }else{
      this.sortDirection = 0;
      this.sortKey = null;
    }
  }

 public toggleBulkEdit(){
   this.bulkEdit = !this.bulkEdit;
    this.edit = {};
  }

 public toggleDelete(){
    let toDelete = Object.keys(this.edit);
    const reallyDelete = toDelete.filter(index => this.edit[index]).map(key => +key);
    while (reallyDelete.length){
      this.data.splice(reallyDelete.pop(), 1);
    }
    this.toggleBulkEdit();
  }

public  removeRow(index){
    this.data.splice(index, 1);
  }

  nextPage(){
    this.page++;
    this.loadData();
  }

  prevPage(){
    this.page--;
    this.loadData;
  }

  goFirst(){
    this.page = 0;
    this.loadData;
  }

  goLast(){
    this.page = this.totalPages - 1;
    this.loadData;
  }
}
